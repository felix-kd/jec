<?php
/**
 * Standard page template
 *
 */
?>

<?php get_header(); ?>

      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="container">
          <div class="row">
            <div class="small-12 columns">
              <? include (THEMEPATH.'includes/partials/_default.php'); ?>
            </div>
          </div>
        </div>
      <?php endwhile; else: ?>
        <p><?php _e('Sorry, we couldn\'t find that page. hgfd'); ?></p>
      <?php endif; ?>


<?php get_footer(); ?>
