<?
$ajax = $_GET['ajax'];
$eventID = $ajax ? $_GET['event'] : get_the_ID();

function show_contents($ajax=false){ ?>
  <div class="row">
    <div class="small-12 columns">
      <div class="clearfix bottom-space event-info">
        <? if($ajax): ?>
        <div class="small-12 medium-12 large-12 columns text-justify small-only-text-center">
          <? $feat_image = wp_get_attachment_url( get_post_thumbnail_id($eventID) ); ?>
          <div class="featured-image" style="background-image:url('<?=$feat_image?>')">
          </div>
          <?
          $type = get_field('event_type',$eventID);
          $type_list = rtrim(implode(', ', $type), ',');
          ?>
          <p class="event-type"><?= $type_list ?></p>
          <p class="event_name"><?= get_the_title($eventID) ?></p>
          <p><?= get_field('event_address',$eventID) ?></p>
          <p class="event_date"><?= get_field('event_date',$eventID) ?></p>

          <?
          $content_post = get_post($eventID);
          $content = $content_post->post_content;
          if($ajax){
            $content = getExcerpt($content,0,100);
          }
          ?>
          <p class="view_more text-right"><a href="<?= get_permalink($eventID)?>">View More >> </a></p>
        </div>
      <? else :?>
        <? $feat_image = wp_get_attachment_url( get_post_thumbnail_id($eventID) ); ?>
        <div class="event-row">
          <div class="medium-4 large-3 show-for-medium-up">
            <div class="featured-image" style="background-image:url('<?=$feat_image?>')">
            </div>
          </div>
          <div class="small-12 medium-8 large-9 small-only-text-center">
            <div class="hide-for-medium-up">
              <div class="featured-image" style="background-image:url('<?=$feat_image?>')">
              </div>
            </div>

            <?
            $type = get_field('event_type',$eventID);
            $type_list = rtrim(implode(', ', $type), ',');
            ?>
            <p class="event-type"><?= $type_list ?></p>
            <p class="event-title"><?= get_the_title($eventID) ?></p>
            <p><?= get_field('event_address',$eventID) ?></p>
            <p class="event-date"><?= get_field('event_date',$eventID) ?></p>
            <?
            $content_post = get_post($eventID);
            $content = $content_post->post_content;
            ?>
            <p><?= apply_filters('the_content', $content) ?></p>
          </div>
        </div>
      <? endif; ?>
      </div>
    </div>
  </div> <?
}
if($ajax){ ?>
  <a class="close-reveal-modal right" aria-label="Close"><i class="fa fa-times"></i></a><?
  show_contents($ajax);
} else { ?>
  <?php get_header(); ?>
  <?
  $date = get_field('event_date', get_the_ID());
  $year = date('Y',strtotime($date));
  ?>
  <div class="row">
    <div class="small-12 columns">

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <h1>EVENTS & PROJECTS</h1>
      <?
        $page = get_page_by_title('EVENTS & PROJECTS');
        $back_link = get_page_link($page->ID) ."?event_year={$year}";
        $back_link = isset($_GET['archive']) ? get_page_link($_GET['archive']) : $back_link;
      ?>
      <nav class="breadcrumbs pull-top show-for-medium-up">
        <a href="<?= $back_link ?>">EVENTS & PROJECTS</a>
        <a class="current" href="#"><?= get_the_title(); ?></a>
      </nav>
      <nav class="breadcrumbs pull-top hide-for-medium-up">
        <a href="<?= $back_link ?>"><i class="fa fa-angle-double-left"></i></a>
        <a class="current" href="#"><?= get_the_title(); ?></a>
      </nav>
      <?
        show_contents(false);
      $thumb_slider = get_field("list_of_images");
      if($thumb_slider) :
        ?><h2 class="under_border">GALLERY</h2><?
        include (THEMEPATH.'includes/partials/_thumbnails.php');
      endif; ?>
    <?php endwhile; else: ?>
      <p><?php _e('Sorry, we couldn\'t find that page.'); ?></p>
    <?php endif; ?>

    </div><!--col-->
  </div><!--row-->

  <?php get_footer(); ?>
<? } ?>
