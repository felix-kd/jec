<!DOCTYPE html>
<!--[if IE 7]>    <html class="no-js ie7 ie-oldie ie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 ie-oldie ie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9 ie" lang="en"> <![endif]-->
<!--[if !IE]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="apple-touch-icon" href="/apple-touch-icon.png">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <title><? wp_title(); ?></title>
    <?php wp_head(); ?>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <script src="<?= JAVASCRIPTPATH ?>/library/production/js/vendor/placeholders.min.js"></script>
      <script src="<?= JAVASCRIPTPATH ?>/library/development/js/vendor/polyfill.min.js"></script>
    <![endif]-->
    <!--[if lt IE 8]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <script src="<?= JAVASCRIPTPATH ?>/library/production/js/vendor/placeholders.min.js"></script>
      <script src="<?= JAVASCRIPTPATH ?>/library/development/js/vendor/polyfill.min.js"></script>
    <![endif]-->

  </head>
  <body <? body_class(); ?>>
    <!-- <header class="show-for-medium-only">
      <div class="row header-nav">
        <div class="medium-9 columns">
          <a class="logo" href="<?= get_home_url(); ?>"><img src="<?= IMAGEPATH."left_logo.png"?>"></a>
        </div>
        <div class="medium-3  columns">
          <section class="tab-bar right">
            <a class="right-off-canvas-toggle right" href="#"><i class="change-ico fa fa-bars"></i></a>
          </section>
        </div>
      </div>
    </header> -->
    <header class="hide-for-large-up">
      <div class="row header-nav">
        <div class="small-10 columns">
          <a class="logo" href="<?= get_home_url(); ?>"><img src="<?= IMAGEPATH."left_logo.png"?>"></a>
        </div>
        <div class="small-2  columns">
          <section class="tab-bar right">
            <a class="right-off-canvas-toggle right" href="#"><i class="change-ico fa fa-bars"></i></a>
            <!-- <a class="menu-icon right-off-canvas-toggle right">
                <span>
                </span>
            </a> -->

          </section>
        </div>
      </div>
    </header>
    <aside class="right-off-canvas-menu">
      <div class="tbl-display">
        <div class="tbl-display-cell text-center">
          <?
            wp_nav_menu( array( 'theme_location' => 'primary_navigation', 'menu_class'=> 'nav side-nav', 'container' => 'ul' ) );
          ?>
        </div>
      </div>
    </aside>
    <div class="off-canvas-wrap" data-offcanvas>
      <div class="inner-wrap">

    <header class="show-for-large-up">
      <div class="row header-nav">
        <div class="small-9 medium-9 large-4 columns">
          <a class="logo" href="<?= get_home_url(); ?>"><img src="<?= IMAGEPATH."left_logo.png"?>"></a>
        </div>
        <div class="large-8  columns">
          <? wp_nav_menu( array( 'theme_location' => 'primary_navigation', 'menu_class'=> 'side-nav', 'container' => 'ul' ) ); ?>
        </div>

      </div>
    </header>



    <section class="main_content">
      <!-- <div class="container page-container"> -->
        <!-- <section class="main-section"> -->
