<?php
/**
 * Template Name: Home page
 *
 */

?>
<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <div class="container">
    <? include (THEMEPATH.'includes/partials/_slider.php'); ?>
    <? include (THEMEPATH.'includes/partials/_events_blocks.php'); ?>
  </div>
<?php endwhile; else: ?>
  <p><?php vp_e('Sorry, we couldn\'t find that post.'); ?></p>
<?php endif; ?>
<?php get_footer(); ?>
