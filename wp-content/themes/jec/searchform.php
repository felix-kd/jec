<form action="<?php echo home_url( '/' ); ?>" method="get" class="form-search">
  <div class="row collapse">
    <div class="small-10 columns">
      <input type="text" name="s" id="search" placeholder="Search" value="<?php the_search_query(); ?>" />
    </div>
    <div class="small-2 columns">
      <a href="#" class="button postfix"><i class="btn-search fa fa-search"></i></a>
    </div>
  </div>
</form>
