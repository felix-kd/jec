<?php

/**********************************************************************************/
/* Register Scripts */
/**********************************************************************************/

// enqueue javascript
if( !function_exists( "theme_js" ) ) {
  function theme_js(){

    // todo: remove from parent
    // deregister_parent_scripts();

    wp_deregister_script('jquery');
    $VERSION = '1.0';
    if (ENVIRONMENT == 'development'){

      wp_register_script( 'jquery',
        JAVASCRIPTPATH.'vendor/jquery-1.12.2.min.js',
        array(),
        $VERSION, true );

      wp_register_script( 'foundation',
        JAVASCRIPTPATH.'vendor/foundation.min.js',
        array('jquery'),
        $VERSION, true );

      wp_register_script( 'modernizr',
        JAVASCRIPTPATH.'vendor/modernizr.js',
        array('jquery'),
        $VERSION, true );

      wp_register_script( 'offcanvas',
        JAVASCRIPTPATH.'vendor/foundation.offcanvas.js',
        array('jquery'),
        $VERSION, true );


      wp_register_script( 'jquery.placeholders',
        JAVASCRIPTPATH.'vendor/placeholders.min.js',
        array('jquery'),
        $VERSION, true );

      wp_register_script( 'backgroundSize',
        JAVASCRIPTPATH.'vendor/jquery.backgroundSize.js',
        array('jquery'),
        $VERSION, true );

      wp_register_script( 'slick',
        JAVASCRIPTPATH.'vendor/slick.js',
        array('jquery'),
        $VERSION, true );

      wp_register_script( 'slick-lightbox',
        JAVASCRIPTPATH.'vendor/slick-lightbox.js',
        array('jquery'),
        $VERSION, true );

      wp_register_script( 'list_pagination',
        JAVASCRIPTPATH.'vendor/jquery.pajinate.js',
        array('jquery'),
        $VERSION, true );

      wp_register_script( 'theme-scripts',
        JAVASCRIPTPATH.'theme-scripts.js',
        array('jquery'),
        $VERSION, true );

      wp_enqueue_script('jquery');
      wp_enqueue_script('foundation');
      wp_enqueue_script('list_pagination');
      wp_enqueue_script('modernizr');
      wp_enqueue_script('offcanvas');
      wp_enqueue_script('jquery.placeholders');
      wp_enqueue_script('backgroundSize');
      wp_enqueue_script('slick');
      wp_enqueue_script('slick-lightbox');


      wp_enqueue_script('theme-scripts');
    }else{
      // wp_register_script( 'jquery-min',
      //   'http://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js',
      //   array(),
      //   $VERSION, true );
      wp_register_script( 'theme-scripts-min',
        JAVASCRIPTPATH.'theme-scripts.min.js',
        array('jquery-min'),
        $VERSION, true );
      // wp_enqueue_script('jquery-min');
      wp_enqueue_script('theme-scripts-min');
    }
  }
}
add_action( 'wp_enqueue_scripts', 'theme_js', 20 );

// Uninitialize any scripts coming from Parent Themes
// function deregister_parent_scripts() {
//   wp_deregister_script( 'jquery' );
//   wp_deregister_script( 'modernizer' );
//   wp_deregister_script( 'responsive-images' );
//   wp_deregister_script( 'bootstrap' );
//   wp_deregister_script( 'load-image' );
//   wp_deregister_script( 'image-gallery' );
//   wp_deregister_script( 'cycle' );
//   wp_deregister_script( 'masonry' );
//   wp_deregister_script( 'jqueryui' );
//   wp_deregister_script( 'klass' );
//   wp_deregister_script( 'photoswipe' );
//   wp_deregister_script( 'mobiscroll' );
//   wp_deregister_script( 'script' );
//   wp_deregister_script( 'gigya' );
// }

?>
