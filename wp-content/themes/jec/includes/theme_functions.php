<?php
/**********************************************************************************/
/* Custom Functions */
/**********************************************************************************/

// example usage:
// $terms = get_the_terms( $latest_post->ID, 'category' );
// echo join(', ', array_map('get_post_term_name', $terms));
// function get_post_term_name($term) {
//   return $term->name;
// }
function getExcerpt($str, $startPos=0, $maxLength=100) {
	if(strlen($str) > $maxLength) {
		$excerpt   = substr($str, $startPos, $maxLength-3);
		$lastSpace = strrpos($excerpt, ' ');
		$excerpt   = substr($excerpt, 0, $lastSpace);
		$excerpt  .= '...';
	} else {
		$excerpt = $str;
	}
	return $excerpt;
}


// temporary function to show array data
function view_array($obj=null){
  echo "<pre>";
  print_r($obj);
  echo "</pre>";
}



add_filter('dynamic_sidebar_params', 'footer_link_widget');

function footer_link_widget( $params ) {


  $widget_name = $params[0]['widget_name'];

	$widget_id = $params[0]['widget_id'];
  $_SESSION['widget_id'] = $widget_id;
  $params[0]['widget_name'] = "Footer Info";

  $widget_id_f = $widget_id;
	if( $widget_name != 'Text' ) {

		return $params;

	}



	// add image to after_widget
	// $image = get_field('image', 'widget_' . $widget_id);

	if( $image ) {
		// $params[0]['after_widget'] = '<img src="' . $image['url'] . '">' . $params[0]['after_widget'];
	}

	// return
	return $params;
}
add_action('admin_head', 'remove_text_title');

function remove_text_title() {
  echo '<style>
    #footer_link_widget .widget-content > p {
      display : none;
    }
  </style>';
}

function sort_year($a, $b)
{
    if ($a == $b) {
        return 0;
    }
    return ($a > $b) ? -1 : 1;
}

function date_compare($a, $b)
{
    $t1 = strtotime($a['date']);
    $t2 = strtotime($b['date']);
    return  $t2 - $t1;
}


// get latest_post
function get_latest($data){
	$new_data = null;
	foreach ($data as $key => $event) {
		$date = get_field('event_date', $event->ID);
		$new_data_temp['data'] = $event;
		$new_data_temp['date'] = $date;
		$new_data[] = $new_data_temp;
		$new_data_temp = null;
	}
	usort($new_data, 'date_compare');
	return array_slice($new_data, 0, 8);
}

function get_more_data($year_n=''){
	$years = array();

  // get posts from WP
  $posts = get_posts(array(
		'post_type'			=> 'jec-events',
		'posts_per_page'	=> -1,
		'meta_key'			=> 'event_date',
		'orderby'			=> 'meta_value_num',
		'order'				=> 'DESC'
  ));

  // loop through posts, populating $years arrays
  foreach($posts as $post) {
		$year = date('Y', strtotime(get_field('event_date', $post->ID)));
		if($year_n <= $year || $year_n == '')
    $years[$year][] = $post;
  }

	return $years;
}


?>
