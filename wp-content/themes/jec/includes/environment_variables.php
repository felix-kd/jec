<?php

/**********************************************************************************/
/* Custom Defines for Environment Paths */
/**********************************************************************************/

DEFINE('THEMEPATH', get_stylesheet_directory().'/');
DEFINE('IMAGEPATH', get_stylesheet_directory_uri().'/library/images/');
DEFINE('AUDIOPATH', get_stylesheet_directory_uri().'/library/audio/');
DEFINE('JAVASCRIPTPATH', get_stylesheet_directory_uri().'/library/'.ENVIRONMENT.'/js/');
DEFINE('PARTIALPATH', get_stylesheet_directory_uri().'/includes/partials/');

?>
