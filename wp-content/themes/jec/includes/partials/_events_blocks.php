<?
$args2 = array(
  'post_type' => 'jec-events',
  'posts_per_page' => -1,
  'orderby' => 'menu_order',
  'order' => 'desc',
  'post_status' => 'publish',
);
$events_list = get_posts($args2);
// view_array() para makita nimo ang sulod sa array or object
// view_array($events_list);
$events_list = get_latest($events_list);
// view_array($events_list2);
if($events_list){
  ?>
  <div class="row">
    <div class="small-12 columns">
      <h3 class="under_border">Recent Projects</h3>
    </div>
  </div>

  <div class="row collapse"><div class="grid clearfix medium-11 medium-centered large-10 large-centered columns"><?
  foreach ($events_list as $key => $event) {
    // foreach ($event_data['data'] as $key => $event) {
    $feat_image = wp_get_attachment_url( get_post_thumbnail_id($event['data']->ID) );
    ?>
    <div class="grid-item small-12 medium-6 large-4 columns">
      <div data-reveal-ajax="<?= rtrim(get_permalink($event['data']->ID), '/'); ?>?ajax=true?event=<?=$event->ID?>" data-reveal-id="event-modal" data-reveal-ajax="true" class="featured-image" style="background-image:url('<?=$feat_image?>'); <?=$feat_image ? '' : 'background-color:#686666;'?>">
        <div class="caption"><?
            $type = get_field('event_type',$event['data']->ID);
            $type_list='';
            if($type)
            $type_list = rtrim(implode(', ', $type), ',');
            ?>

          <p><?= $event['data']->post_title ?></p>
          <p><?= get_field('event_date_front_view',$event['data']->ID) ?></p>
          <p><?= get_field('event_address',$event['data']->ID) ?></p>
          <p><?= $type_list ?></p>

        </div>
        <a class="fill_link" data-reveal-ajax="<?= rtrim(get_permalink($event['data']->ID), '/'); ?>?ajax=true?event=<?=$event->ID?>" data-reveal-id="event-modal" data-reveal-ajax="true"></a>
      </div>
    </div><?
    // }
  }?>
  <div class="grid-item small-12 medium-6 large-4 end columns">
    <div class="featured-image" style="background-image:url('<?= IMAGEPATH ?>project-img9.png')">
      <?
        $see_more_link = get_page_by_title('Events & Projects');
      ?>
      <a class="fill_link" href="<?= get_permalink($see_more_link->ID); ?>"></a>
    </div>
  </div>
  </div></div><?
}
?>
