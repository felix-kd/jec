<?
$year_n = get_field('year_archive',get_page_by_title('Event Archives')->ID);

$year_latest = null;
$years = array();
$data_year = array();
$event_year = isset($_GET['event_year']) ? $_GET['event_year'] : null;
$active_year = null;
$data_year = get_more_data($year_n);
$year_latest = key($data_year);
if($event_year){
  $active_year = $event_year;
} else {
  $active_year = $year_latest;
}
// echo $active_year .'<='. $year_latest.'-'.$year_n;
// $active_year = $year_latest <= $active_year ? 'Archive' : $active_year;

?>

<div class="row">
  <div class="small-12 medium-2 large-1 columns">
    <div class="show-for-small-only pull-top bottom-space">
      <ul class="tabs" data-tab role="tablist">
      <? foreach ($data_year as $key => $year) {
        // if($year < ($year_latest-$year_n)){
        //   $year = 'Archive';
        // }
        ?>
          <li class="tab-title <?= $active_year == $key ? 'active' : '' ?>" role="presentation">
            <a href="#panel_<?= $key ?>" role="tab" tabindex="0" aria-selected="true" aria-controls="panel_<?= $key ?>">
              <?= $key ?>
            </a>
          </li>
      <?
      // if($year == 'Archive')
      // break;
      } ?>
      </ul>
      <ul class="tabs">
      <li class="tab-title" role="presentation">
        <a href="<?= get_permalink(get_page_by_title('Event Archives')) ?>" >
          Archive
        </a>
      </li>
      </ul>
    </div>
    <div class="show-for-medium-up">
      <ul class="tabs" data-tab role="tablist">
      <? foreach ($data_year as $key => $year) {
        // if($year < ($year_latest-$year_n)){
        //   $year = 'Archive';
        // }
        ?>
          <li class="tab-title <?= $active_year == $key ? 'active' : '' ?>" role="presentation">
            <a href="#panel_<?= $key ?>" role="tab" tabindex="0" aria-selected="true" aria-controls="panel_<?= $key ?>">
              <?= $key ?>
            </a>
          </li>
      <?
      // if($year == 'Archive')
      // break;
      } ?>
      </ul>
      <ul class="tabs">
      <li class="tab-title" role="presentation">
        <a href="<?= get_permalink(get_page_by_title('Event Archives')) ?>" >
          Archive
        </a>
      </li>
      </ul>
    </div>
  </div>
  <div class="small-12 medium-10 large-11 columns asd">
    <div class="tabs-content">
      <? $flag = true; foreach ($data_year as $key => $data) {
        $year = $key;
        // if($year < ($year_latest-$year_n)){
        //   $year = 'Archive';
        // }
        ?>
        <section role="tabpanel" aria-hidden="false" class="content <?= $active_year == $year ? 'active' : '' ?>" id="panel_<?= $year ?>">
          <? $flag = false;
          // show data per row
          foreach ($data as $key => $per_event) {
            // view_array($per_event);
            $feat_image = wp_get_attachment_url( get_post_thumbnail_id($per_event->ID) );
             ?>
            <div class="row">
              <div class="small-12 columns">
                <div class="event-row under_border clearfix">
                  <div class="medium-4 large-3 show-for-medium-up">
                    <a class="center-grid" href="<?= get_permalink($per_event->ID)?>"><div class="featured-image" style="background-image:url('<?=$feat_image?>')">
                    </div></a>
                  </div>
                  <div class="small-12 medium-8 large-9 small-only-text-center">
                    <a class="hide-for-medium-up center-grid" href="<?= get_permalink($per_event->ID)?>"><div class="featured-image" style="background-image:url('<?=$feat_image?>')">
                    </div></a>
                    <p class="event-type"><?
                      $type = get_field('event_type',$per_event->ID);
                      $type_list = rtrim(implode(', ', $type), ',');
                      echo $type_list;
                      ?>
                    </p>
                    <!-- <p class="event_name"><?= $per_event->post_title ?></p> -->
                    <p class="event_name"><?= get_the_title($per_event->ID) ?></p>

                    <p><?= get_field('event_address',$per_event->ID) ?></p>
                    <p class="event_date"><?= get_field('event_date_front_view',$per_event->ID) ?></p>
                    <?
                    $content_post = get_post($per_event->ID);
                    $content = $content_post->post_content;
                    $content = getExcerpt($content,0,100);
                    ?>
                    <!-- <div class="show-for-medium-up"><?= apply_filters('the_content', $content) ?></div> -->
                    <a class="tiny-text" href="<?= get_permalink($per_event->ID)?>">Read More</a>
                    <p class="sacer"></p>
                  </div>
                </div>
              </div>
            </div>
            <? } ?>
        </section>
      <? } ?>

    </div>
  </div>
</div>
