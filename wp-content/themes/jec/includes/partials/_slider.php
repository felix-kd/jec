<?
if(is_mobile()){
  $home_slides = get_field('slider_images_mobile');
} elseif (is_tablet()) {
  $home_slides = get_field('slider_images_tablet');
} else {
  $home_slides = get_field('slider_images');
}
// $home_slides = get_field('slider_images');
if( $home_slides ) :
  $arrow = (count($home_slides) > 1 ? true : false);
?>
<div class="home-slides"><ul data-orbit data-options="bullets:false;timer:false;slide_number:false;<?= $arrow ? '' : 'navigation_arrows:false'?>" data-equalizer>
  <? foreach ($home_slides as $key => $images) { ?>
      <li class="img_slider" style="background-image : url('<?= $images['url'] ?>');" data-equalizer-watch>
        <? if($images['caption']) : ?>
        <div class="slider-caption"><?= $images['caption']?></div>
        <? endif; ?>
      </li>
  <? } ?>
</ul></div>
<? endif; ?>
