<div class="row">
  <!-- <div class="medium-centered medium-10 columns"> -->
  <?
  $blocks = get_field("page_block_list");

  ?>
  <div class="small-12 medium-3 large-2 columns">
    <ul class="tabs top-align-section block-page show-for-medium-up"><?
    foreach ($blocks as $key => $block_title) {
      $str_title = str_replace(" ","_",$block_title['block_title']); ?>
      <li class="tab-title <?= $key == 0 ? 'active' : ''?>"><a class="scroll" data-link="block_<?= $str_title ?>" href="#block_<?= $str_title ?>"><?= $block_title['block_title'] ?></a></li>
    <? } ?>
    </ul>
    <div class="nav-sticky">
      <div class="sticky-content">
        <ul class="tab_list text-right"><?
        foreach ($blocks as $key => $block_title) {
          $str_title = str_replace(" ","_",$block_title['block_title']); ?>
          <li class="tab-title <?= $key == 0 ? 'active' : ''?>"><a class="scroll" data-link="block_<?= $str_title ?>" href="#block_<?= $str_title ?>"><?= $block_title['block_title'] ?></a></li>
        <? } ?>
        </ul>
      </div>
      <div class="text-right menu-sticky">
        <span class="circle">
          <i class="fa fa-1x fa-times menu-nav"></i>
        </span>
      </div>
    </div>
  </div>
  <div class="small-12 medium-9 large-10 columns"><?
    foreach ($blocks as $key => $block_section) {
      $str_title = str_replace(" ","_",$block_section['block_title']); ?>
      <section id="block_<?= $str_title ?>" class="contents">
        <h2><?= $block_section['block_title'] ?></h2>
        <? if($block_section['block_description']) : ?>
          <?= $block_section['block_description'] ?>
        <? else : ?>
          <? include (THEMEPATH.'includes/partials/_clients.php'); ?>
        <? endif; ?>

      </section>
    <? } ?>

    <?
    $thumb_slider = get_field("list_of_images");
    if($thumb_slider) :
      include (THEMEPATH.'includes/partials/_thumbnails.php');
    endif; ?>
  </div>
</div>
