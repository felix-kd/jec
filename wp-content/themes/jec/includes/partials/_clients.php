<?
  $clients = $block_section['clients'];
  if($clients){ ?>
    <div class="bg_white add_sace">

    <ul class="small-block-grid-3 medium-block-grid-6 large-block-grid-9 block-clients"> <?
    foreach ($clients as $key => $client) {
      // $client['clients_logo']
      // $client['clients_web_link']
      if($client['clients_logo']){ ?>
        <li>
          <a <?= $client['clients_web_link'] ? "target='_blank'" : ""?> href="<?= $client['clients_web_link'] ? $client['clients_web_link'] : "#"?>">
            <img src="<?= $client['clients_logo']['sizes']['medium'] ?>" alt="<?= $client['clients_logo']['alt'] ?>" title="<?= $client['clients_logo']['title'] ?>">
          </a>
        </li> <?
      }
    }
    ?>
  </ul>
  </div>
    <?
  } else {
    echo 'Sorry No Clients Found';
  }
?>
