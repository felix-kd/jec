<div class="nav-bg">
  <div class="row">
    <div class="small-12 medium-4 columns">
        <a href="<?= home_url( '/' ) ?>"><img src="<?= IMAGEPATH ?>left_logo.png" alt="<?= get_bloginfo('name'); ?>" /></a>
    </div>
    <div class="small-12 medium-8 columns">
      <?php
        wp_nav_menu( array( 'theme_location' => 'primary_navigation', 'menu_class'=> '', 'container' => 'ul' ) );
      ?>
    </div>
  </div>
</div>
<aside class="right-off-canvas-menu">
  <div class="tbl-display">
    <div class="toggle-left-menu">
      <a class="right-off-canvas-toggle" ><i class="fa fa-angle-<?=$direction?>"></i></a>
    </div>
    <div class="tbl-display-cell text-center">
      <a href="<?= home_url( '/' ) ?>"><img src="<?= IMAGEPATH ?>left_logo.png" alt="<?= get_bloginfo('name'); ?>" /></a>
      <?
        wp_nav_menu( array( 'theme_location' => 'primary_navigation', 'menu_class'=> 'nav side-nav', 'container' => 'ul' ) );
      ?>
    </div>
  </div>
</aside>
<section class="right-small">
  <a class="right-off-canvas-toggle menu-icon-ie border-icon-ie" href="#"></a>
</section>
