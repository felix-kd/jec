<?
// check string for "%n%" to replace newline

$title = get_field("alternative_title") ? get_field("alternative_title") : get_the_title();
$title = str_replace("%n%","\r\n",$title);
?>
<h1><?= nl2br(trim($title)); ?></h1>
