<?
// comon page slider array of thumb images
// view_array($thumb_slider);
?>
<div class="row">
  <div class="small-12 columns">
    <div class="clearfix">
      <div class="small-12">
        <section class="regular slider">
          <? foreach ($thumb_slider as $key => $value) { ?>
            <div class="thumb-slide"><a href="<?= $value['sizes']['large'] ?>" target="_blank" class="thumbnail"><img src="<?= $value['sizes']['slider-thumb'] ?>" alt="<?= $value['alt']?>"></a></div>
          <? } ?>
        </section>
      </div>
    </div>
  </div>
</div>
