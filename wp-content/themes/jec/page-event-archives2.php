<?php
/**
 * Template Name: Event Archives 2
 *
 */


?>
<?php get_header(); ?>


<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <div class="container">
    <div class="row">
      <div class="small-12 columns">
        <? include (THEMEPATH.'includes/partials/common/_get_page_title.php'); ?>
        <nav class="breadcrumbs pull-top show-for-medium-up">
          <a href="<?= get_permalink(get_page_by_title('Events & Projects'))?>?event_year=<?=$year?>">EVENTS & PROJECTS</a>
          <a class="current" href="#"><?= get_the_title(); ?></a>
        </nav>
        <nav class="breadcrumbs pull-top hide-for-medium-up">
          <a href="<?= get_permalink(get_page_by_title('Events & Projects'))?>?event_year=<?=$year?>"><i class="fa fa-angle-double-left"></i></a>
          <a class="current" href="#"><?= get_the_title(); ?></a>
        </nav>

        <?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$args = array( 'post_type'=> 'jec-events','posts_per_page' => 5, 'meta_key' => 'event_date', 'order'=> 'DESC', 'orderby' => 'meta_value', 'paged'=>$paged);
$catpost_ = new WP_Query( $args );
if ($catpost_->have_posts() ) :
    while ($catpost_->have_posts() ) : $catpost_->the_post();
    echo get_the_title();
 ?>

<!--- CONTENT --->
<?php
 endwhile;
 endif;
 wp_reset_query();?>
 <?php
             $big=76;
         $args = array(
 'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
 'format'       => '?paged=%#%',
 'total'        => $catpost_->max_num_pages,
 'current'      => $paged,
 'prev_next'    => True,
 'prev_text'    => __('Previous'),
 'next_text'    => __('Next'),

 'type'         => 'list');
 // ECHO THE PAGENATION
 echo paginate_links( $args );
 ?>
      </div>
    </div>
  </div>

<?php endwhile; else: ?>
  <p><?php vp_e('Sorry, we couldn\'t find that post.'); ?></p>
<?php endif; ?>
<?php get_footer(); ?>
