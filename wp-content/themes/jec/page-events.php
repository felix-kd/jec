<?php
/**
 * Template Name: Events & Projects
 *
 */


?>
<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <div class="container">
    <div class="row">
      <div class="small-12 columns">
        <? include (THEMEPATH.'includes/partials/common/_get_page_title.php'); ?>
        <? include (THEMEPATH.'includes/partials/_events.php'); ?>
      </div>
    </div>
  </div>
<?php endwhile; else: ?>
  <p><?php vp_e('Sorry, we couldn\'t find that post.'); ?></p>
<?php endif; ?>

<?php get_footer(); ?>
