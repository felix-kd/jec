<?php
/**
 * Template Name: Contact Us Page
 *
 */


?>
<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <!-- <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCAZVvddHfBPEmj5XDc7S8xsZSgixnPmmg" type="text/javascript"></script>
  <script>
  function initialize() {
    var mapProp = {
      center:new google.maps.LatLng(51.508742,-0.120850),
      zoom:5,
      mapTypeId:google.maps.MapTypeId.ROADMAP
    };
    var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
  }
  google.maps.event.addDomListener(window, 'load', initialize);
  </script> -->
  <div class="container">
    <div class="row">
      <div class="small-12 columns">
      <? include (THEMEPATH.'includes/partials/common/_get_page_title.php'); ?>
      <div class="row">
        <div class="small-12 medium-5 large-4 columns show-for-medium-up">
          <ul class="small-block-grid-1">
            <?
            global $widget_id_global;

              if(is_active_sidebar('footer_link_widget')) :

                ob_start();
                dynamic_sidebar('footer_link_widget');
                $sidebar = ob_get_contents();
                ob_end_clean();

                $widget_id_f = 'widget_'.$_SESSION['widget_id'];
                $information_list = get_field('information_list', $widget_id_f);

            foreach ($information_list as $key => $info) {?>
            <li>
              <div class="row collapse">
                <div class="small-1 columns text-center">
                  <i class=" fa <?= $info['information_label']?>"></i>
                </div>
                <div class="small-10 columns end">
                  <?= $info['information_value']?>
                </div>
              </div>
            </li>
            <? } ?>

            <? endif; // end is_active_sidebar ?>
          </ul>
          <? echo do_shortcode('[contact-form-7 id="4" title="Contact form 1"]'); ?>
        </div>
        <div class="small-12 medium-7 large-8 columns">
          <? if($map = get_field('map')) :?>
            <div class="flex-video">
              <?= html_entity_decode($map); ?>
            </div>
          <? else : ?>
            no map found
          <? endif ?>
        </div>
        <div class="small-12 medium-3 large-4 columns show-for-small-only">
          <ul class="small-block-grid-1">
            <?
            global $widget_id_global;

              if(is_active_sidebar('footer_link_widget')) :

                ob_start();
                dynamic_sidebar('footer_link_widget');
                $sidebar = ob_get_contents();
                ob_end_clean();

                $widget_id_f = 'widget_'.$_SESSION['widget_id'];
                $information_list = get_field('information_list', $widget_id_f);

            foreach ($information_list as $key => $info) {?>
            <li>
              <div class="row collapse">
                <div class="small-2 medium-1 columns text-center">
                  <i class=" fa <?= $info['information_label']?>"></i>
                </div>
                <div class="small-10 medium-11 columns end">
                  <?= $info['information_value']?>
                </div>
              </div>
            </li>
            <? } ?>

            <? endif; // end is_active_sidebar ?>
          </ul>
          <? echo do_shortcode('[contact-form-7 id="4" title="Contact form 1"]'); ?>
        </div>
      </div>
      </div>
    </div>
  </div>

<?php endwhile; else: ?>
  <p><?php vp_e('Sorry, we couldn\'t find that post.'); ?></p>
<?php endif; ?>
<?php get_footer(); ?>
