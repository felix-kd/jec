<?php
/**
 * Template Name: Event Archives
 *
 */


?>
<?php get_header(); ?>


<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <div class="container">
    <div class="row">
      <div class="small-12 columns">
        <? include (THEMEPATH.'includes/partials/common/_get_page_title.php'); ?>
        <nav class="breadcrumbs pull-top show-for-medium-up">
          <a href="<?= get_permalink(get_page_by_title('Events & Projects'))?>">EVENTS & PROJECTS</a>
          <a class="current" href="#"><?= get_the_title(); ?></a>
        </nav>
        <nav class="breadcrumbs pull-top hide-for-medium-up">
          <a href="<?= get_permalink(get_page_by_title('Events & Projects'))?>"><i class="fa fa-angle-double-left"></i></a>
          <a class="current" href="#"><?= get_the_title(); ?></a>
        </nav>
        <div id="listgen">
          <div class="page_navigation "></div>
          <div class="clearfix"></div>

        <ul class="content small-block-grid-1 medium-block-grid-3 large-block-grid-6 block-clients " style="display: flex;flex-wrap: wrap;">
          <?
          $year_n = get_field('year_archive');
          // show data per row
          $data_yearly = get_more_data();
          // view_array($data);
          foreach ($data_yearly as $y => $data) { ?>
          <?
          $track = false;

          if($year_n > $y) :
          foreach ($data as $key => $per_event) {
            // view_array($per_event);
            $feat_image = wp_get_attachment_url( get_post_thumbnail_id($per_event->ID) );
             ?>

             <li style="display: flex;">
                <div class="event-row clearfix panel radius" style="display: flex;width:100%;margin-bottom:0;justify-content:flex-start;flex-direction:column;position:relative;" >

                  <div class="small-12 text-center" style="">
                    <p style="position: absolute;top: 8px;left: 8px;font-size: 12px;"><?= !$track ? $y : ''?></p>
                    <? $track = true; ?>
                    <a class="center-grid" href="<?= get_permalink($per_event->ID)?>?archive=<?=get_the_ID()?>"><div class="featured-image" style="background-image:url('<?=$feat_image?>'); width:100px; height:100px;background-size:cover;">
                    </div></a>
                    <p class="event-type"><?
                      $type = get_field('event_type',$per_event->ID);
                      $type_list = rtrim(implode(', ', $type), ',');
                      echo $type_list;
                      ?>
                    </p>
                    <p class="event_name"><?= get_the_title($per_event->ID) ?></p>

                    <p><?= get_field('event_address',$per_event->ID) ?></p>
                    <p class="event_date"><?= get_field('event_date_front_view',$per_event->ID) ?></p>
                    <?
                    $content_post = get_post($per_event->ID);
                    $content = $content_post->post_content;
                    $content = getExcerpt($content,0,100);
                    ?>


                  </div>
                  <div class="small-12 text-center" style="margin-top:auto;">
                    <a class="tiny-text" href="<?= get_permalink($per_event->ID)?>?archive=<?=get_the_ID()?>">Read More</a>
                  </div>
                </div>
                </li>
                <? }
              endif;
                } ?>
        </ul>
        <div class="page_navigation"></div>
        </div>
      </div>
    </div>
  </div>

<?php endwhile; else: ?>
  <p><?php vp_e('Sorry, we couldn\'t find that post.'); ?></p>
<?php endif; ?>
<?php get_footer(); ?>items_to_show
<script type="text/javascript">
jQuery(function($) {
  $(document).ready(function(){
    $('#listgen').pajinate({
      num_page_links_to_display : 3,
      items_per_page : <?= get_field('items_to_show') ?>,
      abort_on_small_lists: true,
      nav_label_prev : '<',
      nav_label_next : '>'
    });
  });
});
</script>
