    <?
    // $thumb_slider = get_field("list_of_images");
    // if($thumb_slider) :
    //   include (THEMEPATH.'includes/partials/_thumbnails.php');
    // endif; ?>

    </section>
    <footer>
      <div class="row" data-equalizer>
        <div data-equalizer-watch class="medium-5 large-4 small-text-center space-bottom columns">
          <img class="footer_logo" src="<?= IMAGEPATH ?>footer-logo.png">
        </div>
        <div data-equalizer-watch class="medium-7 large-8 columns">
          <div class="flex-f"><ul class="footer-list">
            <?
            global $widget_id_global;

              if(is_active_sidebar('footer_link_widget')) :

                ob_start();
                dynamic_sidebar('footer_link_widget');
                $sidebar = ob_get_contents();
                ob_end_clean();

                // experiment start
                // view_array( $sidebar);

                // experiment end
                $widget_id_f = 'widget_'.$_SESSION['widget_id'];
                $information_list = get_field('information_list', $widget_id_f);

            ?>
            <? foreach ($information_list as $key => $info) {?>
            <li class="row collapse">
              <div class="small-2 medium-1 columns text-center">
                <i class="space-right fa <?= $info['information_label']?>"></i>
              </div>
              <div class="small-10 medium-11 columns end">
                <?= $info['information_value']?>
              </div>
            </li>
            <? } ?>

            <? endif; // end is_active_sidebar ?>
          </ul>
        </div>
        </div>
      </div>
      <div class="footer-bottom">
          <div class="small-12 columns">
            2016 © J Eugenio Concepts & Events, Inc. All Rights Reserved
          </div>
      </div>
    </footer>
    <a class="exit-off-canvas"></a>
  </div>
  </div>

    <script type="text/javascript">
      var IS_MOBILE = "<?= is_mobile(); ?>";
      var IS_TABLET = "<?= is_tablet(); ?>";
      var SITE_URL = ('https:' == document.location.protocol ? 'https://' : 'http://') + document.location.host;
    </script>

    <?php wp_footer(); ?>
    <? include( THEMEPATH.'includes/partials/_tracking.php'); ?>
    <? include (THEMEPATH.'includes/partials/modals/_event_modal.php'); ?>
  </body>
</html>
