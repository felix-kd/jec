// IS_MOBILE defined in footer.php
// IS_TABLET defined in footer.php
// SITE_URL defined in footer.php

// global variables

jQuery(function($) {

  $(document).foundation({
    offcanvas : {
        open_method: 'move'
    },
    tab: {
      callback : function (tab) {
        // console.log(tab);
      }
    }
  });
  $(document).foundation('clearing', 'reflow');
  $(document).ready(function($) {

    // hack for safari off canvas
     $('.fill_link').click(function(){ false });
    // add script below .... orbit function is already in preload

    // off canvas
    $(document).on('open.fndtn.offcanvas', '[data-offcanvas]', function () {
      $('.change-ico').removeClass('fa-bars');
      $('.change-ico').addClass('fa-times');
      $('.right-off-canvas-menu').css('right','276px');
    });

    $(document).on('close.fndtn.offcanvas', '[data-offcanvas]', function () {
      $('.change-ico').removeClass('fa-times');
      $('.change-ico').addClass('fa-bars');
      $('.right-off-canvas-menu').css('right','0');
    });

    // scroll event
    $(".scroll").click(function(){
      var link = $(this).data("link");
      var parent = $(this).parent().parent();
      // parent.removeClass("active");
      parent.find(".active").removeClass("active")
      console.log(parent.find(".active").text());
      $(this).parent().addClass("active");
      $("html,body").animate({
        scrollTop: $("#"+link).offset().top - 80
      },"500");
      return false
    });

    // search
    $(".btn-search").click(function(){
      $(".form-search").submit();
    });

    $(".regular").slick({
      dots: false,
      infinite: true,
      slidesToShow: 6,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
      }]
    });
    $('.regular').slickLightbox();

    $(".menu-sticky").click(function(){
      $(".sticky-content").slideToggle( "slow" );
      if($(".menu-nav").hasClass("fa-times")){
        // $(".sticky-content").animate({height: "0"},100, function(){
          $(".menu-nav").removeClass("fa-times");
          $(".menu-nav").addClass("fa-bars");
        // });
        // $(".sticky-content").slideDown("slow");
      } else {
        // $(".sticky-content").animate({height: "auto"},100, function(){
          $(".menu-nav").removeClass("fa-bars");
          $(".menu-nav").addClass("fa-times");
        // });
      }
      // return false;
    });

    window.addEventListener("scroll", function(event) {
      var top = this.scrollY,
          left =this.scrollX;
          if(top > 85){
            $(".top-align-section").addClass("stick");
          } else {
            $(".top-align-section").removeClass("stick");
          }
  }, false);

  // $('#listgen').pajinate({
  //   nav_label_first : '',
  //   nav_label_last : '',
  //   nav_label_prev : '<',
  //   nav_label_next : '>',
  //   num_page_links_to_display : 3,
  //   items_per_page : 6
  // });

  }).foundation('orbit', 'reflow');// Ready

});// jQuery
