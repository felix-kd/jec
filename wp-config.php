<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'jec_web');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('FS_METHOD','direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '09*)J`1jtiybpw+!zi16*LF>g#[NAKeE#L^NY+{Y(8SJXv1/]R@ryhLPAm<DETAZ');
define('SECURE_AUTH_KEY',  ']ok+[X-D%A0tT^|w~,}McorX|%m;: }fq6vMJSuO35O`o[[wJ?/-B>u1Ns!|ND~J');
define('LOGGED_IN_KEY',    'sM1}g3)(p;u.V/puU44a P;:R#MBW2`k<#VQl3j%MR$nYfcs`QSIFQF8FmziWSS:');
define('NONCE_KEY',        'x[Kh!q<PzG}H(-n)@.,7n#dk(,EZ{}kEtx`n_|Dhnja[]CBX?pm(HfcNRmAB$i<R');
define('AUTH_SALT',        'j.1Q=34Rdxd|mY=5$]Ub?EC:z[BU=zBukX#C2?]UK^I,EH0P`%D*Rkwgi~c6M<@[');
define('SECURE_AUTH_SALT', '3e.y$tc4bMwT1Pb67c;IZPXh+-(shnp]gMp/xN8Iy{.:GBmg`TVuK>!a#z-a_<E(');
define('LOGGED_IN_SALT',   'J1%L?:91!4j34]=Icvb-+~@t/0wtT|ZYe+- |ku`dARIkwGdwzty#R`/kR$]*M 3');
define('NONCE_SALT',       ':[Y9R#UJTu3+ XN*xRk@`by+HA%pN+I:uJ6imUqkB)lLj.?2w#PaYL__#|0&:C_,');

//Theme env var
define('ENVIRONMENT', 'development');

// define('ENVIRONMENT', 'production');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
